<?php

namespace App\Repositories;

use App\User;

/**
 * This Repository give all the information about Users
 */
class UserRepository
{
	/**
	* get all admins
	* @return array 
	*/
	public function getAllAdmins()
	{
		return User::where('is_admin',1)->pluck('email','name');
	}
}