<?php

namespace App\Services;

use App\Repositories\UserRepository;

/**
 * This class will Check Http Status Code and Send Mail to Admin If website go down
 */
class WebsiteStatusCheck
{
	public function __construct()
	{
		$this->user = new UserRepository;
	}

	/**
	* 
	* send message to Admin if wesite is Down
	* @param string content
	* @param to array
	* @return view
	*/
	public function sendmail($to,$content)
	{
		return view('cron',["content" => $content]);
	}

	/**
	*
	* check Http status of Website
	* @param website url
	* @return view 
	*/
	public function status($website)
	{
		$headers = get_headers($website);
		$http_response_code = substr($headers[0], 9, 3);

		// check from Http status Code that Website is Down or Not
		if ( ($http_response_code >= 200 && $http_response_code < 300)
		 || $http_response_code == 301 || $http_response_code == 302 ) { 

			//Website is Up
			$content = "Hello Admin , Site is Up";
			return view('cron',["content" => $content]);
		}
		else{
			//Website is Down
			$admins = $this->user->getAllAdmins();
			$content = "Hello Admin , Site is Down";
			return $this->sendmail($admins,$content); 
		}
	}
}