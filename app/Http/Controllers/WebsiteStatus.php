<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\WebsiteStatusCheck;

class WebsiteStatus extends Controller
{
	/*
    |--------------------------------------------------------------------------
    | WebsiteStatus Controller
    |--------------------------------------------------------------------------
    |
    | This controller Checks the Website is Down or Not
    | If website is down it sends informaition email to all the Admins
    |
    */
	private $url="https://stackoverflow.com/sdafsgr"; 

	public function __construct()
	{
		$this->webstatus = new WebsiteStatusCheck;
	}

	/**
	*Call function to check Status of Website
	*/
    public function check()
    {
    	return $this->webstatus->status($this->url);
    }
}
